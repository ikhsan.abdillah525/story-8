from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

def home(request):
    return render(request, 'main/home.html')

def books(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    req = requests.get(url)
    booklist = json.loads(req.content)

    return JsonResponse(booklist, safe=False)
